﻿using UnityEngine;

public class BlinkScript : MonoBehaviour
{
    public GameObject objectToBlink;

    void Update()
    {
        if(objectToBlink != null)
            objectToBlink.SetActive(Mathf.RoundToInt(Time.time) % 2 == 0);
    }
}
