﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(Renderer))]
[RequireComponent(typeof(MeshCollider))]
[ExecuteInEditMode]
public class MeshCreator : Singleton<MeshCreator>
{

    #region enum
    public enum MAP_TYPE
    {
        EMPTY = 0,
        GRASS = 1,
        STONE = 2,
		BORDER = 3

    }
    #endregion

    #region private var
    private Mesh _meshRef;
    private MeshCollider _meshColRef;
    private List<Vector3> _meshVertices = new List<Vector3>();
    private List<Vector3> _colVertices = new List<Vector3>();
    private List<int> _meshTriangles = new List<int>();
    private List<int> _colTriangles = new List<int>();
    private List<Vector2> _uvs = new List<Vector2>();
    private float _textureRatio = 0.0625f;
    private int _colliderCount;
    private int _faceCount;
    private int _stoneLayer;
    private int _dirtLayer;

    [Range(1, 256)]
    [SerializeField]
    private int s_mapSizeX;
    [Range(1, 256)]
    [SerializeField]
    private int s_mapSizeY;
    [SerializeField]
    private int s_dirtYOffset;
    [SerializeField]
    private float s_stoneAmplitude;
    [SerializeField]
    private float s_dirtAmplitude;
    [SerializeField]
    private float s_holeStrengh;
    [SerializeField]
    private int s_holeWidth;
    [SerializeField]
    private int s_holeHeight;
    [SerializeField]
    private float s_stoneExp;
    [SerializeField]
    private float s_dirtExp;
	[Space(2f)]
	[SerializeField]
	private float s_borderRange;
    #endregion

    #region public var
    public Vector2 grassTexturePos;
    public Vector2 stoneTexturePos;
	public Vector2 borderTexturePos;

    public byte[,] blocks;
	public delegate void ReloadAction();
	public event ReloadAction OnReload;
    #endregion


    #region Unity Methodes
    void Awake()
    {
        _meshRef = GetComponent<MeshFilter>().sharedMesh;
        _meshColRef = GetComponent<MeshCollider>();
    }

    void Start()
    {
        _colliderCount = 0;
        _faceCount = 0;
        GenerateMap();
        RefreshMap();
        //Camera.main.transform.position = new Vector3 ((s_mapSizeX / 2),(s_mapSizeY/2 ),-10);
        //Camera.main.orthographicSize = s_mapSizeY/2;
    }
    #endregion

    #region private methodes
    void BuildMesh(int x, int y, Vector2 texturePos)
    {
        _meshVertices.Add(new Vector3(x, y, 0));
        _meshVertices.Add(new Vector3(x + 1, y, 0));
        _meshVertices.Add(new Vector3(x + 1, y - 1, 0));
        _meshVertices.Add(new Vector3(x, y - 1, 0));

        _meshTriangles.Add(_faceCount * 4);
        _meshTriangles.Add(_faceCount * 4 + 1);
        _meshTriangles.Add(_faceCount * 4 + 3);
        _meshTriangles.Add(_faceCount * 4 + 1);
        _meshTriangles.Add(_faceCount * 4 + 2);
        _meshTriangles.Add(_faceCount * 4 + 3);
        _faceCount++;

        _uvs.Add(new Vector2(texturePos.x * _textureRatio, texturePos.y * _textureRatio + _textureRatio));
        _uvs.Add(new Vector2(texturePos.x * _textureRatio + _textureRatio, texturePos.y * _textureRatio + _textureRatio));
        _uvs.Add(new Vector2(texturePos.x * _textureRatio + _textureRatio, texturePos.y * _textureRatio));
        _uvs.Add(new Vector2(texturePos.x * _textureRatio, texturePos.y * _textureRatio));

    }

    void GenerateMesh()
    {
        Mesh colMesh = new Mesh();
        colMesh.vertices = _colVertices.ToArray();
        colMesh.triangles = _colTriangles.ToArray();
        _meshColRef.sharedMesh = colMesh;

        _colVertices.Clear();
        _colTriangles.Clear();
        _colliderCount = 0;

        if (_meshRef == null)
        {
            _meshRef = new Mesh();
            GetComponent<MeshFilter>().sharedMesh = _meshRef;
        }

        _meshRef.Clear();
        _meshRef.SetVertices(_meshVertices);
        _meshRef.triangles = _meshTriangles.ToArray();
        _meshRef.uv = _uvs.ToArray();
        ;
        _meshRef.RecalculateNormals();

        _meshVertices.Clear();
        _meshTriangles.Clear();
        _uvs.Clear();
        _faceCount = 0;

    }

    void ConstructMesh()
    {
        for (int mx = 0; mx < blocks.GetLength(0); mx++)
        {
            for (int my = 0; my < blocks.GetLength(1); my++)
            {
                if (GetBlockType(mx, my) == (int)MAP_TYPE.GRASS)
                {
                    BuildMesh(mx, my, grassTexturePos);
                    BuildCol(mx, my);
                }
                else if (GetBlockType(mx, my) == (int)MAP_TYPE.STONE)
                {
                    BuildMesh(mx, my, stoneTexturePos);
                    BuildCol(mx, my);
				}
				else if (GetBlockType(mx, my) == (int)MAP_TYPE.BORDER)
				{
					BuildMesh(mx, my, borderTexturePos);
					BuildCol(mx, my);
				}
            }
        }
    }


    void BuildCol(int x, int y)
    {
        Vector3 offset = new Vector3(x, y, 0);
		if (GetBlockType (x, y) != (int)MAP_TYPE.EMPTY) {
			_colVertices.Add (new Vector3 (0, 0, 0) + offset);
			_colVertices.Add (new Vector3 (1, 0, 0) + offset);
			_colVertices.Add (new Vector3 (0, -1, 0) + offset);
			_colVertices.Add (new Vector3 (1, -1, 0) + offset);
			ColliderTriangle ();
		}
			//top
		if (GetBlockType (x, y + 1) == (int)MAP_TYPE.EMPTY) {
			_colVertices.Add (new Vector3 (0, 0, 1) + offset);
			_colVertices.Add (new Vector3 (1, 0, 1) + offset);
			_colVertices.Add (new Vector3 (1, 0, 0) + offset);
			_colVertices.Add (new Vector3 (0, 0, 0) + offset);
			ColliderTriangle ();
		}
			//bot
		if (GetBlockType (x, y - 1) == (int)MAP_TYPE.EMPTY) {
			_colVertices.Add (new Vector3 (0, -1, 0) + offset);
			_colVertices.Add (new Vector3 (1, -1, 0) + offset);
			_colVertices.Add (new Vector3 (1, -1, 1) + offset);
			_colVertices.Add (new Vector3 (0, -1, 1) + offset);
			ColliderTriangle ();
		}

			//left
		if (GetBlockType (x - 1, y) == (int)MAP_TYPE.EMPTY) {
			_colVertices.Add (new Vector3 (0, -1, 1) + offset);
			_colVertices.Add (new Vector3 (0, 0, 1) + offset);
			_colVertices.Add (new Vector3 (0, 0, 0) + offset);
			_colVertices.Add (new Vector3 (0, -1, 0) + offset);
			ColliderTriangle ();
		}
			//right
		if (GetBlockType (x + 1, y) == (int)MAP_TYPE.EMPTY) {
			_colVertices.Add (new Vector3 (1, 0, 1) + offset);
			_colVertices.Add (new Vector3 (1, -1, 1) + offset);
			_colVertices.Add (new Vector3 (1, -1, 0) + offset);
			_colVertices.Add (new Vector3 (1, 0, 0) + offset);
			ColliderTriangle ();
		}

    }

    void ColliderTriangle()
    {
        _colTriangles.Add(_colliderCount * 4);
        _colTriangles.Add(_colliderCount * 4 + 1);
        _colTriangles.Add(_colliderCount * 4 + 3);
        _colTriangles.Add(_colliderCount * 4 + 1);
        _colTriangles.Add(_colliderCount * 4 + 2);
        _colTriangles.Add(_colliderCount * 4 + 3);
        _colliderCount++;
    }
    

	void GenerateBorderBlock(ref byte[,] blocks,int mx, int my){
		float ratio;
		if (mx <= s_borderRange) {
			ratio = s_borderRange / (s_borderRange + mx);
			if (Random.value <= ratio)
				blocks[mx,my] = (int)MAP_TYPE.BORDER;

		}
		if (mx >= (s_mapSizeX-1) - s_borderRange) {
			ratio = ((-s_borderRange -1)/(mx - s_mapSizeX - s_borderRange));
			if (Random.value <= ratio)
				blocks[mx,my] = (int)MAP_TYPE.BORDER;

		}
		if (my <= s_borderRange) {
			ratio = s_borderRange / (s_borderRange + my);
			if (Random.value <= ratio)
				blocks[mx,my] = (int)MAP_TYPE.BORDER;

				
		}
		if (my >= s_mapSizeY - s_borderRange-1) {
			ratio = ((-s_borderRange -1)/(my - s_mapSizeY - s_borderRange));
			if (Random.value <= ratio)
				blocks[mx,my] = (int)MAP_TYPE.BORDER;

		}
	}


    void GenerateDirtLayer(int x)
    {
        _stoneLayer = Noise(x, 0, s_mapSizeY * 0.8f, s_stoneAmplitude, s_stoneExp);
        _stoneLayer += Noise(x, (int)(s_mapSizeY * 0.8f), s_mapSizeY / 2, s_stoneAmplitude * 2, s_stoneExp);
    }


    void GenerateStoneLayer(int x)
    {
        _dirtLayer = Noise(x, 0, s_mapSizeY * 0.6f, s_dirtAmplitude, s_dirtExp);
        _dirtLayer += Noise(x, (int)(s_mapSizeY * 0.6f), s_mapSizeY / 2, s_dirtAmplitude * 2, s_dirtExp);
        _dirtLayer += s_dirtYOffset;
    }


    int Noise(int x, int y, float scale, float mag, float exp)
    {
        return (int)Mathf.Pow(Mathf.PerlinNoise(x / scale, y / scale) * mag, exp);
    }
    #endregion

    #region public methodes
    public void GenerateMap()
    {
        blocks = new byte[s_mapSizeX, s_mapSizeY];

        for (int mx = 0; mx < blocks.GetLength(0); ++mx)
        {
            GenerateStoneLayer(mx);
            GenerateDirtLayer(mx);

            for (int my = 0; my < blocks.GetLength(1); ++my)
            {
                if (my < _stoneLayer)
                {
                    blocks[mx, my] = (int)MAP_TYPE.STONE;
                    if (Noise(mx, my, 14, 16, 1) > 10)
                    {
                        blocks[mx, my] = (int)MAP_TYPE.GRASS;
                    }
                    if (Noise(mx * s_holeHeight, my * s_holeWidth, s_holeStrengh, 16, 1) > 10)
                    {
                        blocks[mx, my] = (int)MAP_TYPE.EMPTY;
                    }
                }
                else if (my < _dirtLayer)
                {
                    blocks[mx, my] = (int)MAP_TYPE.GRASS;
                    if (Noise(mx, my, 14, 16, 1) > 10)
                    {
                        blocks[mx, my] = (int)MAP_TYPE.STONE;
                    }
                    if (Noise(mx * s_holeHeight, my * s_holeWidth, s_holeStrengh, 16, 1) > 10)
                    {
                        blocks[mx, my] = (int)MAP_TYPE.EMPTY;
                    }
                }
                else
                {
                    blocks[mx, my] = (int)MAP_TYPE.EMPTY;
                }
                GenerateBorderBlock(ref blocks, mx, my);
            }
        }
    }


    public void RefreshMap()
    {
        ConstructMesh();
        GenerateMesh();
    }
    
    
    public byte GetBlockType(int x, int y)
    {
        if (x <= -1 || x >= blocks.GetLength(0) || y <= -1 || y >= blocks.GetLength(1))
        {
            return (int)MAP_TYPE.EMPTY;
        }
        return blocks[x, y];
    }

    public void DestroyBlock(Vector2 pos)
    {
        if (IsInBound((int)pos.x, (int)pos.y) && blocks[(int)pos.x, (int)pos.y] != (int)MAP_TYPE.EMPTY)
        {
            blocks[(int)pos.x, (int)pos.y] = (int)MAP_TYPE.EMPTY;
            RefreshMap();
            OnReload();
        }
    }

    public bool IsInBound(int x, int y)
    {
        if (x < 0)
            return false;
        if (x >= s_mapSizeX)
            return false;

        if (y < 0)
            return false;
        if (y >= s_mapSizeY)
            return false;

        return true;
    }

    public void CreateWorld()
    {
        GenerateMap();
        RefreshMap();
    }

	public int GetMapWidth(){
		return s_mapSizeX;
	}

	public int GetMapLength(){
		return s_mapSizeY;
	}
    #endregion
}
