﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Rigidbody))]
public class Enemy : MonoBehaviour
{

    [SerializeField] private float speed;
    [SerializeField] private float minDistToNode;
    public uint AggroRange;

    private Transform target;
    private Transform seeker;
    private Rigidbody seekerRb;

    private Grid grid;
    private List<Node> path = new List<Node>();

    public bool isEnabled = true;

    void Awake()
    {
        grid = GameObject.FindGameObjectWithTag("Grid").GetComponent<Grid>();
        target = GameObject.FindGameObjectWithTag("Player").transform;
        seeker = transform;
        seekerRb = GetComponent<Rigidbody>();
    }

    void Update()
    {
		if (!isEnabled)
            return;

        if (Vector3.Distance(seeker.position, target.position) < AggroRange)
        {
            FindPath(seeker.position, target.position);
            if (path.Count > 1)
            {
                Vector3 nodePos = path[1].worldPosition;
                if (Vector3.Distance(seeker.position, nodePos) >= minDistToNode)
                {
                    Vector3 dir = (nodePos - seeker.position).normalized;
                    Debug.DrawRay(transform.position, dir, Color.red);
                    //seekerRb.AddForce (dir * speed);
                    transform.Translate(dir * speed * Time.deltaTime, Space.World);
                }
            }
        }

    }

    void FindPath(Vector3 startPos, Vector3 targetPos)
    {
        Node startNode = grid.NodeFromWorldPoint(startPos);
        Node targetNode = grid.NodeFromWorldPoint(targetPos);

        List<Node> openSet = new List<Node>();
        HashSet<Node> closedSet = new HashSet<Node>();
        openSet.Add(startNode);

        while (openSet.Count > 0)
        {
            Node node = openSet[0];
            for (int i = 1; i < openSet.Count; i++)
            {
                if (openSet[i].fCost < node.fCost || openSet[i].fCost == node.fCost)
                {
                    if (openSet[i].hCost < node.hCost)
                        node = openSet[i];
                }
            }

            openSet.Remove(node);
            closedSet.Add(node);
            if (node == targetNode)
            {
                RetracePath(startNode, targetNode);
                return;
            }

            foreach (Node neighbour in grid.GetNeighbours(node))
            {
                if (!neighbour.walkable || closedSet.Contains(neighbour))
                {
                    continue;
                }

                int newCostToNeighbour = node.gCost + GetDistance(node, neighbour);
                if (newCostToNeighbour < neighbour.gCost || !openSet.Contains(neighbour))
                {
                    neighbour.gCost = newCostToNeighbour;
                    neighbour.hCost = GetDistance(neighbour, targetNode);
                    neighbour.parent = node;

                    if (!openSet.Contains(neighbour))
                        openSet.Add(neighbour);
                }
            }
        }
    }

    void RetracePath(Node startNode, Node endNode)
    {
        path.Clear();
        Node currentNode = endNode;

        while (currentNode != startNode)
        {
            path.Add(currentNode);
            currentNode = currentNode.parent;
        }
		path.Add(currentNode); //should be the target node
        path.Reverse();

        grid.path = path;
    }

    int GetDistance(Node nodeA, Node nodeB)
    {
        int dstX = Mathf.Abs(nodeA.gridX - nodeB.gridX);
        int dstY = Mathf.Abs(nodeA.gridY - nodeB.gridY);
        if (dstX > dstY)
            return dstY + (dstX - dstY);
        return dstX + (dstY - dstX);
    }

    public void Respawn()
    {
        StartCoroutine(RespawnCoroutine());
    }

    IEnumerator RespawnCoroutine()
    {
        isEnabled = false;
        GetComponentInChildren<SpriteRenderer>().enabled = false;
        yield return new WaitForSeconds(3.0f);
        GetComponentInChildren<SpriteRenderer>().enabled = true;
        isEnabled = true;
    }
}