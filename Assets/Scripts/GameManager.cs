﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;


public class GameManager : Singleton<GameManager>
{
    public GameObject mainMenu;
    public GameObject hud;
    public GameObject endMenu;

    public Text livesText;
    public Text scoreText;

    public Player player;

    uint lives;
    public uint numLives = 3;

	public int score = 0;

    Vector3 startPos;

    private void Start()
    {
        player.enabled = false;
        startPos = player.transform.position;
        lives = numLives;
    }
    
    public void OnPlayButtonClicked()
    {
        endMenu.SetActive(false);
        mainMenu.SetActive(false);
        hud.SetActive(true);

        player.enabled = true;

        lives = numLives;
        livesText.text = "Lives: " + lives;

        score = 0;
        scoreText.text = "Score: " + score;
    }

    public void GameOver()
    {
        lives--;
        livesText.text = "Lives: " + lives;

        if (lives == 0)
        {
            player.enabled = false;
            endMenu.SetActive(true);
            hud.SetActive(false);
            Score.Instance.GenerateScore();
        }
        else
        {
            player.SetBlinkingState();
        }
    }

    public void OnRestartButtonClicked()
    {
        MeshCreator.Instance.GenerateMap();
        MeshCreator.Instance.RefreshMap();
        player.transform.position = startPos;
        OnPlayButtonClicked();
    }


    public void EnemyKilled(Enemy enemy)
    {
        if (enemy.isEnabled)
        {
            score += 100;
            scoreText.text = "Score: " + score;
            enemy.Respawn();
        }
    }
}
