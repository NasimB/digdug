﻿using UnityEngine;
using System.Collections;

public class MouseFocus : Singleton<MouseFocus> {

	[HideInInspector] private Vector3 mousePos;

	void Update () {
		mousePos = Camera.main.ScreenToWorldPoint (Input.mousePosition);
	}

	public Vector2 GetMousePos(){
		return new Vector2 (Mathf.RoundToInt(mousePos.x),Mathf.RoundToInt(mousePos.y));
	}
}
