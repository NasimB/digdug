﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Score : Singleton<Score>
{
    [SerializeField] private Text[] scoreTexts = new Text[10];
	[SerializeField] private int maxScore = 100;
	private List<int> scores = new List<int>();
    private List<string> names = new List<string>();
	private int playerScore = 0;
    private string playerName = "YOU";
	private int iter = 0;

    void Awake()
    {
        for (int i = 0; i < scoreTexts.Length; i++)
        {
            names.Add("AAA");
        }
        for (int i = 0; i < scoreTexts.Length; i++)
        {
			scores.Add(maxScore / (i+1) * 100);
        }
        DisplayScores();
    }


    public void GenerateScore()
    {
        playerScore = GameManager.Instance.score;
        InsertPlayerScore();
        DisplayScores();
    }

    private string BuildText(int rank)
    {
        return names[rank] + "  " + scores[rank];
    }

    private void InsertPlayerScore()
    {
        int playerRank = 0;

        for (int i = scores.Count - 1; i >= 0; i--)
        {
            if (playerScore > scores[i])
            {
                playerRank = i;
            }
		}
        for (int j = scores.Count - 1; j > playerRank; j--)
        {
            scores[j] = scores[j - 1];
            names[j] = names[j - 1];
        }
        scores[playerRank] = playerScore;
        names[playerRank] = playerName;
    }

    private void DisplayScores()
    {
        for (int i = 0; i < scoreTexts.Length; i++)
        {
            scoreTexts[i].text = BuildText(i);
        }
    }
}
