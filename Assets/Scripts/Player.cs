﻿using UnityEngine;

public class Player : MonoBehaviour
{
    private float _speed = 5.0f;

    private float blinkingTime = 0.0f;

    private float attackCooldown = 0.0f;

    private MeshRenderer meshRenderer;
    public SpriteRenderer attackSpriteRenderer;

    private void Start()
    {
        meshRenderer = GetComponent<MeshRenderer>();
    }

    void Update()
    {
        blinkingTime -= Time.deltaTime;
        attackCooldown -= Time.deltaTime;

        attackSpriteRenderer.enabled = attackCooldown > 0;

        meshRenderer.enabled = blinkingTime > 0.0f ? Mathf.RoundToInt(Time.time * 8.0f) % 2 == 0 : true;


        Vector2 posGrid = new Vector2(Mathf.Round(transform.position.x - 0.5f), Mathf.Round(transform.position.z + 0.5f));

        // MOVEMENTS
        if (attackCooldown <= 0.0f)
        {
            if (Input.GetKey(KeyCode.UpArrow))
            {
                if (MeshCreator.Instance.GetBlockType((int)posGrid.x, (int)posGrid.y + 1) != (int)MeshCreator.MAP_TYPE.BORDER)
                {
                    transform.Translate(new Vector3(0, 0, 1) * _speed * Time.deltaTime, Space.World);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Round(transform.position.z - 0.5f) + 0.5f);
                }

                transform.localScale = new Vector3(1, 1, 1);
                transform.rotation = Quaternion.Euler(90, 0, -90.0f);
            }
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                if (MeshCreator.Instance.GetBlockType((int)posGrid.x, (int)posGrid.y - 1) != (int)MeshCreator.MAP_TYPE.BORDER)
                {
                    transform.Translate(new Vector3(0, 0, -1) * _speed * Time.deltaTime, Space.World);
                }
                else
                {
                    transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Round(transform.position.z - 0.5f) + 0.5f);
                }

                transform.localScale = new Vector3(1, 1, 1);
                transform.rotation = Quaternion.Euler(90, 0, 90.0f);
            }
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                if (MeshCreator.Instance.GetBlockType((int)posGrid.x + 1, (int)posGrid.y) != (int)MeshCreator.MAP_TYPE.BORDER)
                {
                    transform.Translate(new Vector3(1, 0, 0) * _speed * Time.deltaTime, Space.World);
                }
                else
                {
                    transform.position = new Vector3(Mathf.Round(transform.position.x - 0.5f) + 0.5f, transform.position.y, transform.position.z);
                }

                transform.localScale = new Vector3(-1, 1, 1);
                transform.rotation = Quaternion.Euler(90, 0, 0.0f);
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                if (MeshCreator.Instance.GetBlockType((int)posGrid.x - 1, (int)posGrid.y) != (int)MeshCreator.MAP_TYPE.BORDER)
                {
                    transform.Translate(new Vector3(-1, 0, 0) * _speed * Time.deltaTime, Space.World);
                }
                else
                {
                    transform.position = new Vector3(Mathf.Round(transform.position.x - 0.5f) + 0.5f, transform.position.y, transform.position.z);
                }

                transform.localScale = new Vector3(1, 1, 1);
                transform.rotation = Quaternion.Euler(90, 0, 0.0f);
            }
        }

        MeshCreator.Instance.DestroyBlock(posGrid);

        // Lock to grid when a button is released
        {
            if (Input.GetKeyUp(KeyCode.UpArrow) || Input.GetKeyUp(KeyCode.DownArrow))
                transform.position = new Vector3(transform.position.x, transform.position.y, Mathf.Round(transform.position.z - 0.5f) + 0.5f);

            if (Input.GetKeyUp(KeyCode.RightArrow) || Input.GetKeyUp(KeyCode.LeftArrow))
                transform.position = new Vector3(Mathf.Round(transform.position.x - 0.5f) + 0.5f, transform.position.y, transform.position.z);
        }


        // ATTACK
        if(Input.GetKey(KeyCode.Space) && attackCooldown <= 0.0f)
        {
            attackCooldown = 0.4f;

            GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
            for(int i = 0; i < enemies.Length; ++i)
            {
                float angle = Vector3.Angle(transform.right * transform.localScale.x * -1.0f, enemies[i].transform.position - transform.position);
                float distance = Vector3.Distance(enemies[i].transform.position, transform.position);
                
                if (distance < 2.0f && Mathf.Abs(angle) < 55.0f)
                {
                    GameManager.Instance.EnemyKilled(enemies[i].GetComponent<Enemy>());
                }
            }
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.CompareTag("Enemy") && blinkingTime < 0.0f && collider.gameObject.GetComponent<Enemy>().isEnabled)
        {
            GameManager.Instance.GameOver();
        }
    }


    public void SetBlinkingState()
    {
        blinkingTime = 1.5f;
    }
}
