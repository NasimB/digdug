﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;


[CustomEditor(typeof(MeshCreator))]
public class MeshCreatorEditor : Editor
{
    private Dictionary<Texture2D, Vector2> TextureAndPosDic = new Dictionary<Texture2D, Vector2>();
    bool foldOutValueGrass;
	bool foldOutValueStone;
    bool foldOutValueBorder;
    const string GRASS = "grass";
    const string STONE = "stone";
	const string BORDER = "border";


    public override void OnInspectorGUI()
    {
        MeshCreator myScript = (MeshCreator)target;
        ButtonWorldGenerate(myScript);
        FillTexture(myScript);
        if (GetFoldOut(ref foldOutValueGrass, GRASS))
        {
            DisplayButton(myScript, GRASS);
        }
        if (GetFoldOut(ref foldOutValueStone, STONE))
        {
            DisplayButton(myScript, STONE);
        }
		if (GetFoldOut(ref foldOutValueBorder, BORDER))
		{
			DisplayButton(myScript, BORDER);
		}

        base.OnInspectorGUI();
    }

    bool GetFoldOut(ref bool foldBool, string name)
    {
        foldBool = EditorGUILayout.Foldout(foldBool, name);
        return foldBool;
    }

    void DisplayButton(MeshCreator myScript, string material)
    {
        EditorGUILayout.BeginHorizontal();
        int count = 0;
        foreach (KeyValuePair<Texture2D, Vector2> entry in TextureAndPosDic)
        {
            if (GUILayout.Button(entry.Key))
            {
                if (material == GRASS)
                {
                    myScript.grassTexturePos = entry.Value;
                    foldOutValueGrass = false;
                }
                else if (material == STONE)
                {
                    myScript.stoneTexturePos = entry.Value;
                    foldOutValueStone = false;
				} 
				else if (material == BORDER)
				{
					myScript.borderTexturePos = entry.Value;
					foldOutValueBorder = false;
				}
            }
            if (count == 3)
            {
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.BeginHorizontal();
                count = 0;
            }
            count++;
        }
        EditorGUILayout.EndHorizontal();
    }

    void ButtonWorldGenerate(MeshCreator myScript)
    {
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Generate World"))
        {
            myScript.CreateWorld();
        }
        EditorGUILayout.EndHorizontal();
    }

    void FillTexture(MeshCreator mc)
    {
        if (TextureAndPosDic.Count == 0)
        {
            MeshRenderer mr = mc.GetComponent<MeshRenderer>();
            if (mr != null)
            {
                Texture2D tx2D = mr.sharedMaterial.mainTexture as Texture2D;
                if (tx2D != null)
                {
                    int width = tx2D.width;
                    int height = tx2D.height;
                    for (int x = 0; x < width; x += 64)
                    {
                        for (int y = 0; y < height; y += 64)
                        {
                            var colors = tx2D.GetPixels(x, y, 64, 64);
                            Texture2D newTx2D = new Texture2D(64, 64);
                            newTx2D.SetPixels(colors);
                            newTx2D.Apply();
                            TextureAndPosDic.Add(newTx2D, new Vector2(x / 64, y / 64));
                        }
                    }
                }
            }
        }
    }
}
