﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor (typeof (Enemy))]
public class FieldOfViewEditor : Editor {

	void OnSceneGUI() {
		Enemy unit = (Enemy)target;
		Handles.color = Color.white;
		Handles.DrawWireArc (unit.transform.position, Vector3.up, Vector3.forward, 360, unit.AggroRange);
	}

}